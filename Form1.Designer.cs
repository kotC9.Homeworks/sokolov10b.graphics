﻿namespace Sokolov10b.Graphics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRangeFrom = new System.Windows.Forms.Label();
            this.groupBoxRangeTitle = new System.Windows.Forms.GroupBox();
            this.textBoxRangeFrom = new System.Windows.Forms.TextBox();
            this.labelRangeTo = new System.Windows.Forms.Label();
            this.textBoxRangeTo = new System.Windows.Forms.TextBox();
            this.buttonFillArray = new System.Windows.Forms.Button();
            this.buttonClearArray = new System.Windows.Forms.Button();
            this.buttonDrawGraph = new System.Windows.Forms.Button();
            this.buttonDrawHistogram = new System.Windows.Forms.Button();
            this.groupBoxArray = new System.Windows.Forms.GroupBox();
            this.textBoxArray = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBoxRangeTitle.SuspendLayout();
            this.groupBoxArray.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRangeFrom
            // 
            this.labelRangeFrom.Location = new System.Drawing.Point(6, 27);
            this.labelRangeFrom.Name = "labelRangeFrom";
            this.labelRangeFrom.Size = new System.Drawing.Size(26, 23);
            this.labelRangeFrom.TabIndex = 1;
            this.labelRangeFrom.Text = "от";
            this.labelRangeFrom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxRangeTitle
            // 
            this.groupBoxRangeTitle.Controls.Add(this.textBoxRangeTo);
            this.groupBoxRangeTitle.Controls.Add(this.labelRangeTo);
            this.groupBoxRangeTitle.Controls.Add(this.textBoxRangeFrom);
            this.groupBoxRangeTitle.Controls.Add(this.labelRangeFrom);
            this.groupBoxRangeTitle.Location = new System.Drawing.Point(532, 12);
            this.groupBoxRangeTitle.Name = "groupBoxRangeTitle";
            this.groupBoxRangeTitle.Size = new System.Drawing.Size(200, 71);
            this.groupBoxRangeTitle.TabIndex = 2;
            this.groupBoxRangeTitle.TabStop = false;
            this.groupBoxRangeTitle.Text = "Диапазон";
            // 
            // textBoxRangeFrom
            // 
            this.textBoxRangeFrom.Location = new System.Drawing.Point(30, 27);
            this.textBoxRangeFrom.Name = "textBoxRangeFrom";
            this.textBoxRangeFrom.Size = new System.Drawing.Size(54, 23);
            this.textBoxRangeFrom.TabIndex = 2;
            this.textBoxRangeFrom.Text = "0";
            // 
            // labelRangeTo
            // 
            this.labelRangeTo.Location = new System.Drawing.Point(90, 27);
            this.labelRangeTo.Name = "labelRangeTo";
            this.labelRangeTo.Size = new System.Drawing.Size(31, 23);
            this.labelRangeTo.TabIndex = 3;
            this.labelRangeTo.Text = "до";
            this.labelRangeTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxRangeTo
            // 
            this.textBoxRangeTo.Location = new System.Drawing.Point(127, 27);
            this.textBoxRangeTo.Name = "textBoxRangeTo";
            this.textBoxRangeTo.Size = new System.Drawing.Size(54, 23);
            this.textBoxRangeTo.TabIndex = 4;
            this.textBoxRangeTo.Text = "350";
            // 
            // buttonFillArray
            // 
            this.buttonFillArray.Location = new System.Drawing.Point(532, 112);
            this.buttonFillArray.Name = "buttonFillArray";
            this.buttonFillArray.Size = new System.Drawing.Size(200, 38);
            this.buttonFillArray.TabIndex = 3;
            this.buttonFillArray.Text = "Заполнить массив";
            this.buttonFillArray.UseVisualStyleBackColor = true;
            this.buttonFillArray.Click += new System.EventHandler(this.buttonFillArray_Click);
            // 
            // buttonClearArray
            // 
            this.buttonClearArray.Location = new System.Drawing.Point(532, 156);
            this.buttonClearArray.Name = "buttonClearArray";
            this.buttonClearArray.Size = new System.Drawing.Size(200, 38);
            this.buttonClearArray.TabIndex = 4;
            this.buttonClearArray.Text = "Очистить поле";
            this.buttonClearArray.UseVisualStyleBackColor = true;
            this.buttonClearArray.Click += new System.EventHandler(this.buttonClearArray_Click);
            // 
            // buttonDrawGraph
            // 
            this.buttonDrawGraph.Location = new System.Drawing.Point(532, 200);
            this.buttonDrawGraph.Name = "buttonDrawGraph";
            this.buttonDrawGraph.Size = new System.Drawing.Size(200, 38);
            this.buttonDrawGraph.TabIndex = 5;
            this.buttonDrawGraph.Text = "Построить график";
            this.buttonDrawGraph.UseVisualStyleBackColor = true;
            this.buttonDrawGraph.Click += new System.EventHandler(this.buttonDrawGraph_Click);
            // 
            // buttonDrawHistogram
            // 
            this.buttonDrawHistogram.Location = new System.Drawing.Point(532, 244);
            this.buttonDrawHistogram.Name = "buttonDrawHistogram";
            this.buttonDrawHistogram.Size = new System.Drawing.Size(200, 38);
            this.buttonDrawHistogram.TabIndex = 6;
            this.buttonDrawHistogram.Text = "Построить гистограмму";
            this.buttonDrawHistogram.UseVisualStyleBackColor = true;
            this.buttonDrawHistogram.Click += new System.EventHandler(this.buttonDrawHistogram_Click);
            // 
            // groupBoxArray
            // 
            this.groupBoxArray.Controls.Add(this.textBoxArray);
            this.groupBoxArray.Location = new System.Drawing.Point(12, 12);
            this.groupBoxArray.Name = "groupBoxArray";
            this.groupBoxArray.Size = new System.Drawing.Size(514, 71);
            this.groupBoxArray.TabIndex = 7;
            this.groupBoxArray.TabStop = false;
            this.groupBoxArray.Text = "Массив";
            // 
            // textBoxArray
            // 
            this.textBoxArray.Location = new System.Drawing.Point(25, 27);
            this.textBoxArray.Name = "textBoxArray";
            this.textBoxArray.Size = new System.Drawing.Size(483, 23);
            this.textBoxArray.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 89);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(514, 418);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 519);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBoxArray);
            this.Controls.Add(this.buttonDrawHistogram);
            this.Controls.Add(this.buttonDrawGraph);
            this.Controls.Add(this.buttonClearArray);
            this.Controls.Add(this.buttonFillArray);
            this.Controls.Add(this.groupBoxRangeTitle);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxRangeTitle.ResumeLayout(false);
            this.groupBoxRangeTitle.PerformLayout();
            this.groupBoxArray.ResumeLayout(false);
            this.groupBoxArray.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label labelRangeFrom;
        private System.Windows.Forms.GroupBox groupBoxRangeTitle;
        private System.Windows.Forms.TextBox textBoxRangeFrom;
        private System.Windows.Forms.Label labelRangeTo;
        private System.Windows.Forms.Button buttonClearArray;
        private System.Windows.Forms.Button buttonFillArray;
        private System.Windows.Forms.TextBox textBoxRangeTo;
        private System.Windows.Forms.Button buttonDrawGraph;
        private System.Windows.Forms.GroupBox groupBoxArray;
        private System.Windows.Forms.Button buttonDrawHistogram;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxArray;
    }
}