﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sokolov10b.Graphics
{
    public partial class Form1 : Form
    {
        private const int PointsCount = 10;
        private const int EllispeWidth = 10;
        
        //шаг по x
        private int _stepX;
        private List<Point> _points = new List<Point>();
        private System.Drawing.Graphics _graphics;

        private Pen _redPen = new Pen(Color.Red, 2);
        private Pen _bluePen = new Pen(Color.RoyalBlue, 1);

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // коррекция размера (до ближайших кратных 10 значений)
            pictureBox1.Width = pictureBox1.Width - (pictureBox1.Width % 10);
            pictureBox1.Height = pictureBox1.Height - (pictureBox1.Height % 10);
            pictureBox1.Update();

            _stepX = pictureBox1.Width / PointsCount;
            
        }

        private void buttonFillArray_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(textBoxRangeFrom.Text, out var from) || !int.TryParse(textBoxRangeTo.Text, out var to))
            {
                MessageBox.Show("В поле диапазона неверные данные");
                return;
            }

            if (from > to)
            {
                MessageBox.Show("неверный формат данных (число 'от' больше числа 'до')");
                return;
            }

            // генерация массива
            var random = new Random();

            textBoxArray.Text = "";

            var list = new List<int>();
            for (var i = 0; i < PointsCount; i++)
            {
                //добавление элемента
                list.Add(random.Next(from, to + 1));

                //вывод через пробел:
                textBoxArray.Text += $"{list.Last()} ";
            }

            _points = PointsFromList(list, _stepX);
        }

        private void buttonClearArray_Click(object sender, EventArgs e)
        {
            _graphics.Clear(pictureBox1.BackColor);
            //рисуем сетку
            DrawGrid(_graphics);
        }

        private void buttonDrawGraph_Click(object sender, EventArgs e)
        {
            if (_points.Count == 0)
            {
                MessageBox.Show("Массив не заполнен");
                return;
            }


            //первая точка
            _graphics.FillEllipse(new SolidBrush(Color.Red),
                //coordinates
                _points[0].X, _points[0].Y,
                //width
                EllispeWidth, EllispeWidth);

            //остальные
            for (var index = 1; index < _points.Count; index++)
            {
                _graphics.FillEllipse(new SolidBrush(Color.Red), _points[index].X, _points[index].Y,
                    EllispeWidth, EllispeWidth);

                _graphics
                    .DrawLine(_redPen,
                        // смещение линии по центру
                        new Point(_points[index - 1].X + EllispeWidth / 2, _points[index - 1].Y + EllispeWidth / 2),
                        new Point(_points[index].X + EllispeWidth / 2, _points[index].Y + EllispeWidth / 2)
                        );
            }
        }

        private void buttonDrawHistogram_Click(object sender, EventArgs e)
        {
            if (_points.Count == 0)
            {
                MessageBox.Show("Массив не заполнен");
                return;
            }

            var startX = _stepX / 4;
            var widthX = _stepX / 2;

            for (var index = 0; index < _points.Count; index++)
            {
                var point = _points[index];
                _graphics.FillRectangle(new SolidBrush(RandomColor()), point.X + startX, point.Y, widthX, pictureBox1.Height);
                _graphics.DrawString($"{index}", new Font(FontFamily.GenericMonospace, 10.0f), new SolidBrush(Color.Black), point.X + widthX, point.Y - 15);
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            _graphics = pictureBox1.CreateGraphics();
            DrawGrid(e.Graphics);
        }

        /// <summary>
        /// отрисовка сетки (жесткая привязка к pictureBox1)
        /// </summary>
        private void DrawGrid( System.Drawing.Graphics graphics)
        {
            var x = _stepX;
            
            //по x
            while(x < pictureBox1.Width)
            {
                graphics.DrawLine(_bluePen, 
                    new Point(x, 0),
                    new Point(x, pictureBox1.Height)
                );

                x += _stepX;
            }

            var y = pictureBox1.Height;
            while (y > 0)
            {
                graphics.DrawLine(_bluePen, 
                    new Point(0, y),
                    new Point(pictureBox1.Width, y)
                );
                
                y -= _stepX;
            }
            
            // X , Y 
            graphics.DrawString("Y", new Font(FontFamily.GenericMonospace, 10.0f), new SolidBrush(Color.Black),15,15);
            graphics.DrawString("X", new Font(FontFamily.GenericMonospace, 10.0f), new SolidBrush(Color.Black),pictureBox1.Width - 25,pictureBox1.Height -25);
        }
        
        private int PictureBox1YFromGlobal(int y)
        {
            var offsetY = EllispeWidth;
            return pictureBox1.Height - y - offsetY;
        }

        private Color RandomColor()
        {
            var color = Color.FromArgb( LockedRandom.GetRandomNumber(0,256), 
                                        LockedRandom.GetRandomNumber(0,256), 
                                        LockedRandom.GetRandomNumber(0,256));
            return color;
        }


        
        /// <summary>
        /// get points list from list y's with same step
        /// </summary>
        /// <param name="list"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        private List<Point> PointsFromList(List<int> list, int step)
        {
            var points = new List<Point>();
            var x = 0;
            foreach (var y in list)
            {
                points.Add(new Point(x, PictureBox1YFromGlobal(y)));
                x += step;
            }

            return points;
        }
    }
}