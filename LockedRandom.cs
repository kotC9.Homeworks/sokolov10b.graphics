﻿using System;

namespace Sokolov10b.Graphics
{
    public static class LockedRandom
    {
        private static readonly Random _random = new Random();
        public static int GetRandomNumber(int min, int max)
        {
            lock(_random) // synchronize
            {
                return _random.Next(min, max);
            }
        }
    }
}